# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 105060003's Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
    
* Other functions (add/delete)
    1. 點擊左上角的"105060003's Forum"字樣可以回到首頁。
    2. 點擊右上角的按鈕會出現"Login"字樣，再點擊後會進入到登入/註冊頁面。
    3. 在登入/註冊頁面可以用email&密碼進行註冊或是登入，也可以直接用Google帳戶進行登入，登入成功後會直接回到首頁。
    4. 登入時若是有勾選"remember me"的話，那在關掉頁面後不會登出，如果沒有勾選的話，那在關掉頁面後，網頁會直接登出，所以重新開啟時就要再登入。
    5. 若是在登入狀態下，點擊右上角的按鈕會有使用者的email和"Logout"按鈕，點擊"Logout"的話會登出，點擊email的話會進入到使用者個人資料頁面(user page)。
    6. 在個人資料頁面裡，有頭貼照片、Email、綽號、關於我的資料顯示，若要修改可以按左下角的"我想修改"按鈕，之後就可以上傳自己的頭貼照片檔案，修改自己的綽號以及關於我的內容，修改完成後就可以按"確認送出"按鈕進行個人資料的修改上傳。
    7. 在首頁裡，有四個論壇版，分別是韓星版，清大版，八卦版，電影版，分別點進去後會進入到此版，在此板會顯示曾經發佈過的貼文串(post list page)，再點想看的貼文後，會來到此貼文的文章內容頁面，要注意的是:必須要登入才會看到貼文串!
    8. 文章內容頁面會看到文章分類、文章標題、發文者的頭貼、發文者的email，發文者的綽號(如果他有的話)、發文的時間以及發文的內容。
    9. 文章的下面有回覆區，在推文回覆欄位裡打上回覆內容，再按"送出留言"按鈕，就可以成功在此文章下回覆(leave comment under any post)，然後會顯示回覆者的email&回復時間。
    10. 除此之外，可以發現發文者的email有連結，那是連接到"PO文者的個人資料"頁面，可以查看發文者的個人資料，但不能幫他修改!重點是!!"關於我"那欄不會只是寫心酸的!別人可以看到~
    11. 回到各版的貼文串頁面，點擊右上角會出現"PO文"按鈕，點擊後可以到發文頁面(post page)，要注意的是:必須要登入後才能進到發文頁面!
    12. 發文頁面有分類、標題、內容讓發文者發文，標題和內容皆不可空白，這樣點擊"送出"按鈕後才會成功發文。
    13. 成功發文後會有chrome notification，如果一開始沒有同意發送通知的話，會請求權限，請求成功後透過chrome通知訊息告知你成功發文，然後再直接點擊chrome通知的話，會新開一個你當初發文的那個板的貼文串頁面，讓你看有沒有出現你剛剛的發文(最新貼文會在最下面)。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：
https://midterm-project-4ab36.firebaseapp.com/

## Components Description : 

*Basic Components:
1. Membership Mechanism : 可以使用email和密碼註冊/登入
2. Firebase Page : 有成功deploy出網址在上面
3. Database : 發文/回復/個人資料皆使用到database，且in authenticated way
4. RWD : 可以順利隨著螢幕的大小縮放
5. Topic Key Function : 有做出user page、post page、post list page、leave comment under any post

*Advanced Components:
1. Third-Party Sign In : 可以用Google帳戶登入
2. Chrome Notification : 成功發文後會有(上面有說了)
3. Use CSS Animation : 首頁一進去，每個版都會有顏色變化
4. Security Report : 寫在下面



## Other Functions Description(1~10%) : 
幾乎全部都寫在上面Topic的Other functions (add/delete)那裡了
1. 點擊左上角的"105060003's Forum"字樣可以回到首頁。
2. 登入時若是有勾選"remember me"的話，那在關掉頁面後不會登出，如果沒有勾選的話，那在關掉頁面後，網頁會直接登出，所以重新開啟時就要再登入。
3. 有用到firebase storage儲存頭貼相片
4. 可以修改user page資料
5. 文章內容頁面會看到文章分類、文章標題、發文者的頭貼、發文者的email，發文者的綽號(如果他有的話)、發文的時間以及發文的內容。
6. 文章下的回覆有顯示回覆者的email&回復時間以及回覆內容。
7. 發文者的email有超連結，那是可以連接到"PO文者個人資料"頁面，可以查看發文者的個人資料，但不能幫他修改!
8. 點擊chrome通知的話，會新開一個你當初發文的那個板的貼文串頁面，讓你看有沒有出現你剛剛的發文(最新貼文會在最下面)。


## Security Report (Optional)
在可以輸入文字內容的地方，直接輸入html的code(ex:<b><h1&gt;Hello</h1&gt;</b>)的話不會被插入網頁中。<br>
實作方式:<br>
1.個人資料頁面之所以不會被外界插入html code是因為顯示資料的方式是使用 document.getElementById('xxxx').value = xxxx;而不是 document.getElementById('xxxx').innerHTML = xxxx。<br>
2.發文內容或文章標題也不會被外界插入html code，儘管是使用document.getElementById('xxxx').innerHTML = xxxx的方法，但是因為我在存入database之前，有先把讀取到的內容字串中的"<"和">"取代成Character entities(ex:& l t ;)，換行\n也改成< br >，如此一來就能順利存入正確的文章資料進database。
<br>另外，database資料的讀寫都要登入後才行:<br>
{<br>
  "rules": {<br>
    ".read": "auth != null",<br>
    ".write": "auth != null"<br>
  }<br>
}<br>


## Reference
* https://www.w3schools.com/
* https://getbootstrap.com/ 
