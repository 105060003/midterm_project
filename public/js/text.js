function init() {
    var user_email = '';
    var auth = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dmenu');
        // Check user login
        if (user) {
            user_email = user.email;
            auth = true;
            menu.innerHTML = "<a class='nav-link' id='gotouserpage'>"+user.email+"</a><a class='nav-link' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    console.log('Sign Out!');
                    alert('Sign Out!');
                    window.location = "index.html";
                })
                .catch(function (error) {
                    console.log('Sign Out Error!')
                    alert('Sign Out Error!');
                });
            });

            var gotouserpage = document.getElementById('gotouserpage');
            gotouserpage.addEventListener('click', function () {
                window.location = "user_page.html";
            });


        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='nav-link' href='signin.html' id='dmenu'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
            auth = false;
        }
    });
    var which = getWhich("which");
    console.log("which = " + which);

    function getWhich(varname)
    {
        // First, we load the URL into a variable
        var url = window.location.href;
        // Next, split the url by the ?
        var qparts = url.split("?");
        // Check that there is a querystring, return "" if not
        if (qparts.length == 0)
        {
                return "";
         }
        // Then find the querystring, everything after the ?
        var query = qparts[1];
        // Split the query string into variables (separates by &s)
        var vars = query.split("&");
        // Initialize the value with "" as default
        var value = "";
        // Iterate through vars, checking each one for varname
        for (i=0;i<vars.length;i++)
        {
            // Split the variable by =, which splits name and value
             var parts = vars[i].split("=");
            // Check if the correct variable
            if (parts[0] == varname)
            {
                // Load value into variable
                 value = parts[1];
                // End the loop
                 break;
            }
        }
 
        // Convert escape codes
        value = unescape(value);
        // Return the value
        return value;
    }   

    //發表回覆按鈕
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    

    post_btn.addEventListener('click', function () {
       var date = new Date();
       txt = post_txt.value.replace(/>/g,"&gt;");
       txt = txt.replace(/</g,"&lt;");
       console.log("txt = "+ txt);
        date = date.toLocaleString();
        console.log("回復時間"+date);
        if (post_txt.value != "") {
            firebase.database().ref(which+'/reply_list').push({
                user_email: user_email,
                //post_data: post_txt.value.replace(/(<([^>]+)>)/ig,""),
                post_data: txt,
                post_date: date
                });
            post_txt.value = "";
        }
    });

    //讀回應
    var postsRef = firebase.database().ref(which+'/reply_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                first_count++;
                //console.log("first_count = "+ first_count);
                var data = childSnapshot.val().post_data;
                var email = childSnapshot.val().user_email;
                var date = childSnapshot.val().post_date;
                total_post.push('<div style="border: 1px solid #F8F8FF; border-radius:4px; background-color:#F8F8FF"><p><span class="ml-2" style="color:#808080;font-size:13px;">'+email+'&ensp;&ensp;回覆時間:'+date+'</span><p class="ml-2">'+data+'</p></div><br>');
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            
            postsRef.on('child_added', function (child_added) {
                second_count ++;
                //console.log("second_count = "+ second_count);
                //console.log("child_added: " + child_added.val().post_data);
                if (second_count> first_count) {
                    
                    var data = child_added.val().post_data;
                    var email = child_added.val().user_email;
                    var date = child_added.val().post_date;
    
                    total_post.push('<div style="border: 1px solid #F8F8FF; border-radius:4px; background-color:#F8F8FF"><p><span class="ml-2" style="color:#808080;font-size:13px;">'+email+'&ensp;&ensp;回覆時間:'+date+'</span><p class="ml-2">'+data+'</p></div><br>');
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    
                }
            });

        })
        .catch(e => console.log(e.message));

        //讀文章
        var text = [];
        var textRef = firebase.database().ref(which);
        textRef.once('value')
        .then(function (snapshot) {
            
            var post_class = snapshot.val().class;
            var post_content = snapshot.val().content;
            var post_email = snapshot.val().email;
            var post_title = snapshot.val().title;
            var post_date = snapshot.val().date;
            var post_userID= snapshot.val().userID;
            console.log("class = "+post_class);
            console.log("content = "+post_content);
            console.log("email = "+post_email);
            console.log("title = "+post_title);
            console.log("date = "+post_date);
            console.log("userID = "+post_userID);
            //text.push('<h5 class="ml-2">['+post_class+']&ensp;&ensp;'+post_title+'</h5><p style="border-top:1px gray solid;width:100%"></p><h6 class="pl-2 m-2" style="font-size:12px"><img class="mr-1" src="test.png" alt="" width="5%">作者:'+post_email+'&ensp;&ensp;'+post_date+'</h6><p class="pl-2 m-2">'+post_content+'</p>');
            
            document.getElementById('one').innerHTML = '['+post_class+']&ensp;&ensp;'+post_title;
            document.getElementById('four').innerHTML = '發文時間: '+post_date;
            document.getElementById('three').innerHTML = post_content;
            document.getElementById('two').innerHTML = "作者: "+'<a href="'+'user_page_custom.html?which='+post_userID+'">'+post_email+'</a>';
            
            //讀發文者ID
            
            var Ref = firebase.database().ref('/users/'+post_userID);
            Ref.once('value')
            .then(function (snapshot) {
                
                var smallName ="" ;
                smallName = snapshot.val().smallName;
                if(smallName == "")
                console.log("hi~~~~~~~~~~~~~~");
                console.log("smallName = "+ smallName);
                //document.getElementById('two').innerHTML = "作者: " + smallName;
                document.getElementById('two').innerHTML += ' (綽號: '+smallName+')';
               
            })
            .catch(e => console.log(e.message));
            
             //從雲端下載頭貼
             var storageRef = firebase.storage().ref();
             storageRef.child('/users/'+post_userID).getDownloadURL().then(function (url) {
                 document.getElementById('userImage').src = url;
             }).catch(function (error) {
                 console.log(error.message);
             });

        })
        .catch(e => console.log(e.message));
}


window.onload = function () {
    init();
};

