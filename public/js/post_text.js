function init() {

    var user_id = '';
    var user_email = '';
    var auth = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dmenu');
        // Check user login
        if (user) {
            user_id = user.uid;
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' id='gotouserpage'>"+user.email+"</a><a class='nav-link' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            auth = true;
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    console.log('Sign Out!');
                    alert('Sign Out!');
                    window.location = "index.html";
                })
                .catch(function (error) {
                    console.log('Sign Out Error!')
                    alert('Sign Out Error!');
                });
            });
            var gotouserpage = document.getElementById('gotouserpage');
            gotouserpage.addEventListener('click', function () {
                window.location = "user_page.html";
            });



        } else {
            auth = false;
            // It won't show any post if not login
            menu.innerHTML = "<a class='nav-link' href='signin.html' id='dmenu'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var post_class = "新聞";

    document.getElementById("classfy").addEventListener('change', function() {
        post_class = this.value;
        console.log("分類 = " + post_class);
    }, false);

    var which="";
    //console.log("which = " + which);
    which = getWhich("which");
    console.log("which = " + which);

      var reference = "";
      reference = which+'/posts';
      console.log("reference = "+ reference);
      

    var post_btn = document.getElementById('post_btn');
    var post_content = document.getElementById('content');
    var post_title = document.getElementById('title');
    
    var newpostref = firebase.database().ref(reference);

    post_btn.addEventListener('click', function () {
        var temp = new Date();
        var date = "";
        date = temp.toLocaleString();
        var content = post_content.value.replace(/>/g,"&gt;");
        content = content.replace(/</g,"&lt;");
        content = content.replace(/\n/g,"<br>");
        var title = post_title.value.replace(/>/g,"&gt;");
        title = title.replace(/</g,"&lt;");
        title = title.replace(/\n/g,"<br>");
    
        console.log("存進去前 " + date);
        console.log("存進去前 " + document.getElementById('content').value);
        console.log("存進去前 " +document.getElementById('title').value);
        console.log("存進去前 " +user_id);
        console.log("存進去前 " +post_class);
        if (post_content.value != "" && post_title.value != "" && auth == true) {
            
            
            newpostref.push({
                email: user_email,
                class: ""+post_class+"",
                title: ""+title+"",
                content:""+content+"",
                date: ""+date+"",
                userID: user_id
            })
            .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode );
                console.log(errorMessage);
                
              });
            
              //alert('成功PO文^^');
              notifyMe();
           
            post_content.value = "";
            post_title.value = "";
           
        }
        else if(auth == false)
        {
            alert("請先登入後才能PO文");
        }
        else{
            alert("請確認標題、內容皆不為空白!");
        }
    });


function getWhich(varname)
{
   // First, we load the URL into a variable
   var url = window.location.href;
   // Next, split the url by the ?
   var qparts = url.split("?");
   // Check that there is a querystring, return "" if not
   if (qparts.length == 0)
   {
     return "";
   }
   // Then find the querystring, everything after the ?
   var query = qparts[1];
   // Split the query string into variables (separates by &s)
   var vars = query.split("&");
   // Initialize the value with "" as default
   var value = "";
   // Iterate through vars, checking each one for varname
   for (i=0;i<vars.length;i++)
   {
     // Split the variable by =, which splits name and value
     var parts = vars[i].split("=");
     // Check if the correct variable
     if (parts[0] == varname)
     {
       // Load value into variable
       value = parts[1];
       // End the loop
       break;
     }
   }
 
   // Convert escape codes
   value = unescape(value);
   // Return the value
   return value;
}

      /**************************************************************************/
      var go = "";
      if(which == "KoreanStar")
      {
        go = "korean_star.html?which=KoreanStar";
      }
      else if(which == "NTHU")
      {
        go = "NTHU.html?which=NTHU";
      }
      else if(which == "Gossiping")
      {
        go = "Gossiping.html?which=Gossiping";
      }
      else if(which == "movie")
      {
        go = "movie.html?which=movie";
      }
      
      function notifyMe() {
        if (!("Notification" in window)) {
            alert("成功PO文啦^^可以回去版上查看惹");
          }
        else if (Notification.permission !== "granted")
        {
          //Notification.requestPermission();
          Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification("成功PO文啦^^可以回去版上查看惹");
                notification.onclick = function () {
                  window.open(go);   
                }
            }
            else{
                alert("成功PO文啦^^可以回去版上查看惹");  
            }
          });
        }
        else {
          var notification = new Notification("成功PO文啦^^可以回去版上查看惹");
      
          notification.onclick = function () {
            window.open(go);      
          };
      
        }
      }
      /**************************************************************************/
     

   
}


window.onload = function () {
    init();
};



