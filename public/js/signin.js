function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        console.log("press login button");
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(){
            alert("Login success!!");
            window.location = "index.html";
            console.log("login success");
        } )
        .catch(function (error) {                // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage);
            console.log("login in error");
            //txtEmail.value = "";
            //txtPassword.value = "";
        });
    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            
            alert("success sign in with google!");
            window.location = "index.html";
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode= error.code;
            var errorMessage= error.message;
            alert(errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
            });
    });

    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
         firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function () {
            console.log("success", "You could sign in  right now!");
            alert("success","Sign up success!!");
            txtEmail.value = "";
            txtPassword.value = "";
        })
        .catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("sign up error");
            alert(errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });
}



window.onload = function () {
    initApp();
};

var remember_me = true;
function myFunction(){
  if(remember_me == true){
      remember_me = false;
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
  .then(function() {
    // Existing and future Auth states are now persisted in the current
    // session only. Closing the window would clear any existing state even
    // if a user forgets to sign out.
    // ...
    // New sign-in will be persisted with session persistence.
    return firebase.auth().signInWithEmailAndPassword(email, password);
  })
  .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
  });
  }
  else
      remember_me = true;

  console.log("remember_me = " + remember_me);
}






  

