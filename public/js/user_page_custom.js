function init() {
    var user_email = '';
    var user_id = '';
    var auth = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dmenu');
        // Check user login
        if (user) {
            user_id = user.uid;
            user_email = user.email;
            
            auth = true;
            menu.innerHTML = "<a class='nav-link' id='gotouserpage'>"+user.email+"</a><a class='nav-link' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    console.log('Sign Out!');
                    alert('Sign Out!');
                    window.location = "index.html";
                })
                .catch(function (error) {
                    console.log('Sign Out Error!')
                    alert('Sign Out Error!');
                });
            });
            var gotouserpage = document.getElementById('gotouserpage');
            gotouserpage.addEventListener('click', function () {
                window.location = "user_page.html";
            });



        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='nav-link' href='signin.html' id='dmenu'>Login</a>";
            
            //document.getElementById('post_list').innerHTML = "";
            auth = false;
        }
    });

    
    var which = getWhich("which");
    console.log("which = " + which);

    function getWhich(varname)
    {
        // First, we load the URL into a variable
        var url = window.location.href;
        // Next, split the url by the ?
        var qparts = url.split("?");
        // Check that there is a querystring, return "" if not
        if (qparts.length == 0)
        {
                return "";
         }
        // Then find the querystring, everything after the ?
        var query = qparts[1];
        // Split the query string into variables (separates by &s)
        var vars = query.split("&");
        // Initialize the value with "" as default
        var value = "";
        // Iterate through vars, checking each one for varname
        for (i=0;i<vars.length;i++)
        {
            // Split the variable by =, which splits name and value
             var parts = vars[i].split("=");
            // Check if the correct variable
            if (parts[0] == varname)
            {
                // Load value into variable
                 value = parts[1];
                // End the loop
                 break;
            }
        }
 
        // Convert escape codes
        value = unescape(value);
        // Return the value
        return value;
    }   





             //讀發文者資料
    
             var Ref = firebase.database().ref('/users/'+which);

             Ref.once('value')
             .then(function (snapshot) {
                 console.log(snapshot.val());
                 var smallName ;
                 var WantToSay ;
                 //console.log("under ref userid = " +user_id);
         
                 smallName = snapshot.val().smallName;
                 WantToSay = snapshot.val().WantToSay;
                 postEamil = snapshot.val().user_email;
         
                 document.getElementById('smallName').value = smallName;
                 document.getElementById('WantToSay').value = WantToSay;
                 document.getElementById('_email').value = postEamil;
         
 
             })
             .catch(e => console.log(e.message));
 
             //從雲端下載發文者頭貼
             var storageRef = firebase.storage().ref();
             storageRef.child('/users/'+which).getDownloadURL().then(function (url) {
                 document.getElementById('user_image').src = url;
             }).catch(function (error) {
                 console.log(error.message);
             });
 

  
}


window.onload = function () {
    init();
};

