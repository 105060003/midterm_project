function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dmenu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' id='gotouserpage'>"+user.email+"</a><a class='nav-link' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    console.log('Sign Out!');
                    alert('Sign Out!');
                    window.location = "index.html";
                })
                .catch(function (error) {
                    console.log('Sign Out Error!')
                    alert('Sign Out Error!');
                });
            });
            var gotouserpage = document.getElementById('gotouserpage');
            gotouserpage.addEventListener('click', function () {
                window.location = "user_page.html";
            });


        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='nav-link' href='signin.html' id='dmenu'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
        }
    });

 
  

 
}


window.onload = function () {
    init();
};

