function init() {
    var user_email = '';
    var auth = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dmenu');
        // Check user login
        if (user) {
            auth = true;
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' id='gotouserpage'>"+user.email+"</a><a class='nav-link' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    console.log('Sign Out!');
                    alert('Sign Out!');
                    window.location = "index.html";
                })
                .catch(function (error) {
                    console.log('Sign Out Error!')
                    alert('Sign Out Error!');
                });
            });
            var gotouserpage = document.getElementById('gotouserpage');
            gotouserpage.addEventListener('click', function () {
                window.location = "user_page.html";
            });


        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='nav-link' href='signin.html' id='dmenu'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
            auth = false;
        }
    });

    document.getElementById("post").addEventListener('click', function () {
        if(auth == true)
            window.location='post_text.html?which=movie';
        else
            alert("請先登入後再發文!");
    });

    var which = getWhich("which");
    console.log("which = " + which);

    function getWhich(varname)
    {
        // First, we load the URL into a variable
        var url = window.location.href;
        // Next, split the url by the ?
        var qparts = url.split("?");
        // Check that there is a querystring, return "" if not
        if (qparts.length == 0)
        {
                return "";
         }
        // Then find the querystring, everything after the ?
        var query = qparts[1];
        // Split the query string into variables (separates by &s)
        var vars = query.split("&");
        // Initialize the value with "" as default
        var value = "";
        // Iterate through vars, checking each one for varname
        for (i=0;i<vars.length;i++)
        {
            // Split the variable by =, which splits name and value
             var parts = vars[i].split("=");
            // Check if the correct variable
            if (parts[0] == varname)
            {
                // Load value into variable
                 value = parts[1];
                // End the loop
                 break;
            }
        }
 
        // Convert escape codes
        value = unescape(value);
        // Return the value
        return value;
    }   
    var postsRef = firebase.database().ref(which+'/posts');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                first_count++;
                console.log("first_count = "+ first_count);
                var post_class = childSnapshot.val().class;
                var post_content = childSnapshot.val().content;
                var post_email = childSnapshot.val().email;
                var post_title = childSnapshot.val().title;
                console.log("class = "+post_class);
                console.log("content = "+post_content);
                console.log("email = "+post_email);
                console.log("title = "+post_title);
                var _src =""
                if(post_class == "閒聊"){
                    _src+="chat";
                }
                else if(post_class == "心得"){
                    _src+="heart";
                }
                else if(post_class == "新聞"){
                    _src+="news";
                }
                else if(post_class == "問題"){
                    _src+="question";
                }

                total_post.push('<a href="text.html?which='+which+'/posts/'+childSnapshot.key+'"><div class="d-flex align-items-center p-3 my-3  rounded box-shadow" style="background-color:#D3D3D3;"><img class="mr-3" src="'+_src+'.png" alt="" width="5%"><h6 class="mb-0 lh-100" style="font-size:15px;color:black;" >['+post_class+']&ensp;&ensp;</h6><h6 class="mb-0 lh-100" style="font-size:15px;color:black;">'+post_title+'</h6></div></a>');
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            
            /*postsRef.on('child_added', function (child_added) {
                second_count ++;
                console.log("second_count = "+ second_count);
                console.log("child_added: " + child_added.val().post_data);
                if (second_count> first_count) {
                    
                    var data = child_added.val().post_data;
                    var email = child_added.val().user_email;
    
                    total_post.push();
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    
                }
            });*/

        })
        .catch(e => console.log(e.message));

    
}


window.onload = function () {
    init();
};

